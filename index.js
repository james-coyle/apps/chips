const express = require('express')
const app = express()
const fetch = require('node-fetch')
const fontkit = require('fontkit')
// const bodyParser = require('body-parser')
const boolParser = require('express-query-boolean')

// // parse application/x-www-form-urlencoded
// app.use(bodyParser.urlencoded({ extended: false }))

// // parse application/json
// app.use(bodyParser.json())

// fetch booleans in query string
app.use(boolParser())

app.use((req, res, next) => {
	fetch(req.protocol + '://' + req.get('host') + '/fonts/Verdana.ttf')
		.then((result) => {
			return result.buffer()
		})
		.then((buffer) => {
			global.font = fontkit.create(buffer)
			next()
		})
})

// handle data requests
app.use('/custom', require('./endpoints/custom'))

app.use('/chrome', require('./endpoints/chrome'))
app.use('/npm', require('./endpoints/npm'))
app.use('/nuget', require('./endpoints/nuget'))

// // serve default page
// app.use('*', (req, res) => {
// 	res.sendFile('./serve.html', { root: __dirname })
// })

module.exports = app
