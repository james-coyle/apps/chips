/**
 * Responds with the chip which is defined by the chipOptions.
 *
 * @param {Object} res The response object to respond to.
 * @param {Object} chipOptions The configuration of the chip.
 * @param {String} [chipOptions.label] The primary label text to display on the chip.
 * @param {String} [chipOptions.meta] The secondary meta text to display on the chip.
 * @param {Boolean} [chipOptions.dark] Whether dark theme is enabled.
 * @param {Number} [chipOptions.scale] A percentage value used to determine the color of the meta section. Red->Green if set, Blue if not set.
 * @param {Object} [chipOptions.icon] The icon to display with the chip.
 * @param {String} chipOptions.icon.path The SVG path which renders the icon (24x24).
 * @param {String} chipOptions.icon.color The color of the icon path.
 * @param {String} chipOptions.icon.fill The color of the icon background.
 */
function RespondWithChip(res, chipOptions) {
	res.set('Cache-Control', 's-maxage=600,max-age=60')
	res.set('Content-Type', 'image/svg+xml')
	res.end(GetChip(chipOptions))
}

/**
 * Responds with a chip which is in an error state.
 *
 * @param {Object} res The response object to respond to.
 * @param {Boolean} dark Whether dark theme is enabled.
 * @param {String} [message='Chip configured incorrectly'] The error message to show.
 */
function RespondWithError(res, dark, message = 'Chip configured incorrectly') {
	RespondWithChip(res, {
		icon: {
			path:
				'M11,15H13V17H11V15M11,7H13V13H11V7M12,2C6.47,2 2,6.5 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4A8,8 0 0,1 20,12A8,8 0 0,1 12,20Z',
			color: 'white',
			fill: '#F44336',
		},
		meta: message,
		dark,
		scale: 0,
	})
}

function GetChip(options) {
	const padding = 12

	let textColor = 'black'
	let labelColor = '#E0E0E0'
	let metaColor = '#64B5F6'
	let scaleColors = ['#EF9A9A', '#FFCC80', '#FFF59D', '#C5E1A5', '#A5D6A7']

	let width = 0
	let iconVector = ''
	let labelVector = ''
	let metaVector = ''

	// set theme colors
	if (options.dark) {
		textColor = 'white'
		labelColor = '#424242'
		metaColor = '#1565C0'
		scaleColors = ['#C62828', '#EF6C00', '#F9A825', '#558B2F', '#2E7D32']
	}

	// set meta color
	if (typeof options.scale === 'number') {
		metaColor = scaleColors[Math.floor((options.scale / 100) * 4)] || scaleColors[4]
	}

	// create icon vector
	if (
		typeof options.icon === 'object' &&
		options.icon !== null &&
		typeof options.icon.path === 'string' &&
		typeof options.icon.color === 'string' &&
		typeof options.icon.fill === 'string'
	) {
		width += 32
		iconVector += `<rect width="${width}" height="32" rx="24" ry="16" fill="${options.icon.fill}" />`
		iconVector += `<path transform="scale(0.8) translate(8,8)" d="${options.icon.path}" fill="${options.icon.color}" />`
	}

	// create label vector
	if (typeof options.label === 'string') {
		const textPos = width + padding
		width += padding + GetWidth(options.label) + padding
		labelVector += `<rect width="${width}" height="32" rx="16" ry="16" fill="${labelColor}" />`
		labelVector += `<text x="${textPos}" y="20" fill="${textColor}" opacity="0.8">${options.label}</text>`
	}

	// create meta vector
	if (typeof options.meta === 'string') {
		const textPos = width + padding
		width += padding + GetWidth(options.meta) + padding
		metaVector += `<rect width="${width}" height="32" rx="16" ry="16" fill="${metaColor}" />`
		metaVector += `<text x="${textPos}" y="20" fill="${textColor}" opacity="0.8">${options.meta}</text>`
	}

	return `
		<svg xmlns="http://www.w3.org/2000/svg" height="32" width="${width}">
			<clipPath id="clip-path">
				<rect width="100%" height="32" rx="16" fill="#fff"/>
			</clipPath>
			<g clip-path="url(#clip-path)" font-family="Verdana,sans-serif" font-size="14">
				${metaVector}
				${labelVector}
				${iconVector}
			</g>
		</svg>
	`
}

function GetWidth(string) {
	const layout = global.font.layout(string)
	const ppu = 14 / global.font.unitsPerEm
	return layout.advanceWidth * ppu
}

module.exports = {
	RespondWithChip,
	RespondWithError,
}
