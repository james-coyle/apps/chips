const router = require('express').Router()
const cors = require('cors')
const fetch = require('node-fetch')
const { RespondWithChip } = require('../functions')
router.use(cors())

router.route('/:type/*').get((req, res) => {
	const package = req.params[0]
	const options = {
		icon: {
			path:
				'M17.67 21.633a3.995 3.995 0 1 1 0-7.99 3.995 3.995 0 0 1 0 7.99m-7.969-9.157a2.497 2.497 0 1 1 0-4.994 2.497 2.497 0 0 1 0 4.994m8.145-7.795h-6.667a6.156 6.156 0 0 0-6.154 6.155v6.667a6.154 6.154 0 0 0 6.154 6.154h6.667A6.154 6.154 0 0 0 24 17.503v-6.667a6.155 6.155 0 0 0-6.154-6.155M3.995 2.339a1.998 1.998 0 1 1-3.996 0 1.998 1.998 0 0 1 3.996 0',
			color: 'white',
			fill: '#004880',
		},
		dark: req.query.dark,
	}

	switch (req.params.type) {
		case 'version':
			version(res, package, options)
			break

		case 'downloads':
			downloads(res, package, options)
			break

		default:
			RespondWithError(res, req.query.dark)
			break
	}
})

router.route('*').get((req, res) => {
	RespondWithError(res, req.query.dark)
})

module.exports = router

function getPackageData(package) {
	return fetch('https://api.nuget.org/v3/index.json')
		.then((result) => result.json())
		.then((result) => {
			return result.resources.find((item) => item['@type'])['@id']
		})
		.then((url) => fetch(url + '?q=packageid:' + package.toLowerCase()))
		.then((response) => response.json())
		.then((response) => response.data[0])
}

function version(res, package, options) {
	getPackageData(package)
		.then((response) => {
			return response.version
		})
		.then((version) => {
			options.label = 'Version'
			options.meta = version.toString()
			RespondWithChip(res, options)
		})
}

function downloads(res, package, options) {
	getPackageData(package)
		.then((response) => response.totalDownloads || response.totaldownloads)
		.then((downloads) => {
			options.label = 'Downloads'
			options.meta = downloads.toString()
			RespondWithChip(res, options)
		})
}
