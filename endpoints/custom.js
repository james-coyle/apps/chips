const router = require('express').Router()
const cors = require('cors')
const { RespondWithChip, RespondWithError } = require('../functions')

router.use(cors())

router.route('/:label/:meta').get((req, res) => {
	RespondWithChip(res, {
		icon: null,
		label: req.params.label,
		meta: req.params.meta,
		dark: req.query.dark,
	})
})

router.route('*').get((req, res) => {
	RespondWithError(res, req.query.dark)
})

module.exports = router
