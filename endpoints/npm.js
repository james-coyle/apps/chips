const router = require('express').Router()
const cors = require('cors')
const fetch = require('node-fetch')
const { RespondWithChip, RespondWithError } = require('../functions')
router.use(cors())

router.route('/:type/*').get((req, res) => {
	const package = req.params[0]
	const options = {
		icon: {
			path:
				'M0 7.334v8h6.666v1.332H12v-1.332h12v-8H0zm6.666 6.664H5.334v-4H3.999v4H1.335V8.667h5.331v5.331zm4 0v1.336H8.001V8.667h5.334v5.332h-2.669v-.001zm12.001 0h-1.33v-4h-1.336v4h-1.335v-4h-1.33v4h-2.671V8.667h8.002v5.331zM10.665 10H12v2.667h-1.335V10z',
			color: 'white',
			fill: '#CB3837',
		},
		dark: req.query.dark,
	}

	switch (req.params.type) {
		case 'version':
			version(res, package, options)
			break

		case 'downloads':
			downloads(res, package, options)
			break

		default:
			RespondWithError(res, req.query.dark)
			break
	}
})

router.route('*').get((req, res) => {
	RespondWithError(res, req.query.dark)
})

module.exports = router

function version(res, package, options) {
	fetch('https://registry.npmjs.org/' + package)
		.then((response) => response.json())
		.then((response) => response['dist-tags'].latest)
		.then((version) => {
			options.label = 'Version'
			options.meta = version.toString()
			RespondWithChip(res, options)
		})
}

function downloads(res, package, options) {
	fetch('https://api.npmjs.org/downloads/range/1000-01-01:3000-01-01/' + package)
		.then((response) => response.json())
		.then((response) => response.downloads.map((item) => item.downloads).reduce((total, day) => total + day))
		.then((downloads) => {
			options.label = 'Downloads'
			options.meta = downloads.toString()
			RespondWithChip(res, options)
		})
}
